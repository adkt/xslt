<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template match="/">
	<html>
		<head>
			<link rel="stylesheet" href="css/index.css" type="text/css"/>
			<link rel="stylesheet" href="css/bootstrap.min.css" type="text/css"/>
		</head>
		<body>
			<h1>Address Book</h1>
			<div class="container">
			<div class="mainContainer">
				<xsl:for-each select="Book/Page">
				<xsl:sort select="Name"/>
					<div class="personContainer">
						<table class="personTable">
							<tr>
								<td>
									<xsl:attribute name="class">name</xsl:attribute>
									<xsl:value-of select="Name"/>
								</td>
								<td>
								</td>
							</tr>
							<tr>
								<td>
									<xsl:attribute name="class">phone</xsl:attribute>
									ph. 
									<xsl:value-of select="Phone"/>
								</td>
							</tr>
							<tr>
								<td>
									<xsl:attribute name="class">address</xsl:attribute>
									<xsl:value-of select="Address"/>,
									<xsl:value-of select="State"/>,
									<xsl:value-of select="Country"/>
								</td>
							</tr>
						</table>
					</div>
				</xsl:for-each>
			</div>
			</div>
		</body>
	</html>
</xsl:template>

</xsl:stylesheet>
