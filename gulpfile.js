var gulp = require('gulp');
var less = require('gulp-less');

gulp.task('cssTask', function(){
    return gulp.src('css/less/*.less')
    .pipe(less().on('error', function(err){
        // log error
        this.emit('end');
    }))
    .pipe(gulp.dest('css/'))
});

gulp.task('watch', function() {
    gulp.watch('css/less/*.less', ['cssTask'])
});

//gulp.task('default', [ 'css' ]);
